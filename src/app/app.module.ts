import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injector } from '@angular/core';
import { FutureComponent } from "./components/future/future.component";
import { createCustomElement } from '@angular/elements';

@NgModule({
  declarations: [
    FutureComponent
  ],
  imports: [
    BrowserModule,
  ],
  providers: [],
  bootstrap: [],
  entryComponents: [FutureComponent]
})
export class AppModule {
  constructor(private injector: Injector) {}

  ngDoBootstrap(){
        // Convert `PopupComponent` to a custom element.
        const futureComponent = createCustomElement(FutureComponent, {injector : this.injector});
        // Register the custom element with the browser.
        customElements.define('think-future', futureComponent);
      }
}
