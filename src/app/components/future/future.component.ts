import {
  Component,
  Input
} from '@angular/core';
@Component({
  selector: 'think-future',
  template: './future.html'
})
export class FutureComponent {
  @Input() answer:string;

  saveAnswer = () => {
    alert("Save me !! " + this.answer);
  }
}
